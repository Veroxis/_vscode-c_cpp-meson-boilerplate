# VSCode C/CPP Meson Boilerplate

## Plugins

### General Recommendations

- Todo+ (Fabio Spampinato)
- Better Comments (Aaron Bond)
- Path Intellisense (Christian Kohler)

### CPP

- C/C++ (Microsoft)
- C/C++ Themes (Microsoft)

### Buildsystem

- Meson (Ali Sabil)
- Task Runner (Sana Ajani)

## HowTo

Configure VsCode Action Buttons to execute meson building commands. For an example take a look at .vscode/settings.json
